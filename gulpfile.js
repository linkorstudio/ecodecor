'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css');

var path = {
  build: {
    html: 'build/',
    js: 'build/js/',
    css: 'build/css',
    img: 'build/img'
  },
  src: {
    html: 'src/*.html',
    js: 'src/js/main.js',
    style: 'src/style/main.scss'
  },
  watch: {
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    style: 'src/style/**/*.scss'
  }
};

// Tasks
gulp.task('html:build', function() {
  gulp.src(path.src.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html));
});

gulp.task('js:build', function() {
  gulp.src(path.src.js)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.js));
});

gulp.task('style:build', function() {
  gulp.src(path.src.style)
    .pipe(sass())
    .pipe(prefixer())
    .pipe(cssmin())
    .pipe(gulp.dest(path.build.css));
});

gulp.task('build', [
  'html:build',
  'js:build',
  'style:build'
]);

gulp.task('watch', function() {
  watch([path.watch.html], function(event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
})

gulp.task('default', ['build']);
