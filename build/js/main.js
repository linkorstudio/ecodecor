$(document).ready(function() {
  $('.cat-grid .card-item').hover(
    function() {
      $(this).removeClass('out').addClass('in');
    },
    function() {
      $(this).removeClass('in').addClass('out');
    }
  );

  $('#list-view').click(function() {
    $('.products')
      .removeClass('grid-view')
      .addClass('list-view');
  });
  $('#grid-view').click(function() {
    $('.products')
      .removeClass('list-view')
      .addClass('grid-view');
  });

  // Quantity input
  $('.quant-group button').click(function() {
    var btnData = parseInt($(this).attr('btn-data'));
    $(this).siblings('input').val(function() {
      var value = parseInt($(this).val());
      value += btnData;
      return value;
    });
  });
});